var Navigation = function() {

    var handleSubMenu = function() {
        $('.topmenu a').click(function(){
            $('.collapse').removeClass('show');
            let target = $(this).data('target');
            $(target).addClass('show');
       });
    }

    var handleMenu = function() {
        $('[data-toggle=collapse]').click(function (e) {
            $('[data-toggle=collapse]').parent('li').removeClass('active');
            $(this).parent('li').toggleClass('active');
        });
    }

    return {
        init: function() {
            handleSubMenu();
            handleMenu();
        }
    };
}();
$(document).ready(function() {
    Navigation.init();
});

