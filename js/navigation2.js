var Navigation = function() {

    var handleMenu = function() {
        $('[data-toggle=collapse]').click(function (e) {
            $('[data-toggle=collapse]').parent('li').removeClass('active');
            $(this).parent('li').toggleClass('active');
        });
    }

    return {
        init: function() {
            handleMenu();
        }
    };
}();
$(document).ready(function() {
    Navigation.init();
    document.addEventListener("click", function (e) {
        if($(e.target).closest('[data-toggle=collapse]').length === 0)
        {
            $('[data-toggle=collapse]').parent('li').removeClass('active');
        }
    });
});

